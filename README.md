# fl-ignosi

# HANDS DOWN THE BEST PROJECT IN THE COUNTRY


#DEPLOYMENT DETAILS

This project was made using two Docker images linked together by a Jenkins Pipeline pulling from a public BitBucket repo.

The main app can be run with the image at this address:

172.30.1.1:5000/tp-ignosi/ignosi-app

and the MySQL database can be run with the image at this address:

172.30.1.1:5000/tp-ignosi/ignosi-mysql



### The application image will take the following environmental variables:


| **Description**               | **Environmental Name**  |  **Default** |
| ----------------------------- |-------------------------|--------------|
| Application Code Logging Level| APP_LOG_LEVEL           |   INFO       |              
|Internal Server Port           | SERVER_PORT             |   8081       |
| Logging Root Level            | ROOT_LOG_LEVEL          |   INFO       |
| Spring Framework              | SPRING_LOG_LEVEL        |   INFO       |




### The Mysql image will take the following environmental variables:


| **Description**               | **Environmental Name**  |  **Default** |
| ----------------------------- |-------------------------|--------------|
| Database Host                 | DB_HOST                 |   localhost  |              
| Database Host  Port           | DB_PORT                 |   3306       |
| Database Schema Name          | DB_SCHEMA               |   traderdb   |
| Database User name            | DB_USER                 |  root        |
| Database Password             | DB_PASS                 |  c0nygre     |
| Active MQ Broker              | MQ_HOST                 |   floridalinux23.conygre.com   |
| Active MQ Port                | MQ_PORT                 |  8080        |
| Active MQ Username            | MQ_USER                 |  admin       |
| Active MQ Password            | MQ_PASS                 |  admin       |
| Max size of price table       | PRICE_TABLE_SIZE        |  1000        |
