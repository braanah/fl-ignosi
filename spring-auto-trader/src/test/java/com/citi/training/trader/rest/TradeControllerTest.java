package com.citi.training.trader.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(TradeController.class)
@ActiveProfiles("h2test")
public class TradeControllerTest {

    private static final Logger logger =
                LoggerFactory.getLogger(TradeControllerTest.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeService mockTradeService;
    
    
    @Test
    public void findAllStocks_returnsList() throws Exception {
        when(mockTradeService.findAll())
            .thenReturn(new ArrayList<Trade>());

        MvcResult result = this.mockMvc
                .perform(get("/trade"))
                .andExpect(status().isOk())
                .andReturn();

        logger.info("Result from tradeService.findAll: "
                    + result.getResponse().getContentAsString());
    }  
    
    @Test
    public void createTrade_returnsCreated() throws Exception {
        Trade testTrade = new Trade(new Stock(1, "JAMMIN"), 56, 1, Trade.TradeType.BUY, 2);

        when(mockTradeService
                .save(any(Trade.class)))
            .thenReturn(testTrade.getId());

        this.mockMvc.perform(
                post("/trade" )
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testTrade)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id")
                    .value(testTrade.getId()))
                .andReturn();
        logger.info("Result from Create Trade");
    }

    @Test
    public void deleteTrade_returnsNoContent() throws Exception {
        MvcResult result = this.mockMvc
                .perform(delete("/trade" + "/1"))
                .andExpect(status().isNoContent())
                .andReturn();

        logger.info("Result from TradeService.delete: "
                    + result.getResponse().getContentAsString());
    }
    
    @Test
    public void findTrade_returnsOk() throws Exception {
        MvcResult result = this.mockMvc
                .perform(get("/trade" + "/1"))
                .andExpect(status().isOk())
                .andReturn();

        logger.info("Result from TradeService.findById: "
                    + result.getResponse().getContentAsString());
    }
    
    
}
