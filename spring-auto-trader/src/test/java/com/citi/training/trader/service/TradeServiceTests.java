package com.citi.training.trader.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.strategy.TwoMovingAverages;


@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class TradeServiceTests {
	@Autowired
	private StockService stockService;
	
	@Autowired
	private TwoMovingAveragesService strategyService;
	
	@Autowired
	private TradeService tradeService;
	
	@MockBean
	private TradeDao mockTradeDao;
	
	@Test
	public void test_saveRuns() {
		int newId = 1;
		
		Stock stock = new Stock(-1, "GOOGL");
		stockService.create(stock);
		
		TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
		strategyService.create(strategy);
		
		Trade trade = new Trade(stock, 95.33, 120, Trade.TradeType.BUY, strategy.getId());
		
		when(mockTradeDao.save(trade)).thenReturn(newId);
		int createdId = tradeService.save(trade);
		
		verify(mockTradeDao).save(trade);
		assertEquals(newId, createdId);
	}

	@Test
	public void test_deleteRuns() {
		tradeService.deleteById(4);
		
		verify(mockTradeDao).deleteById(4);
	}
	
	@Test
	public void test_findByIdRuns() {
		int idToFind = 1;
		Stock stock = new Stock(-1, "GOOGL");
		TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
		Trade testTrade = new Trade(stock, 95.33, 120, Trade.TradeType.BUY, strategy.getId());
		
		when(mockTradeDao.findById(idToFind)).thenReturn(testTrade);
		Trade tradeToFind = tradeService.findById(idToFind);
		
		verify(mockTradeDao).findById(idToFind);
		assertEquals(testTrade, tradeToFind);
	}
	
	@Test
	public void test_findByStateRuns() {
		Trade.TradeState stateToFind = Trade.TradeState.INIT;
		
		Stock stock = new Stock(-1, "GOOGL");
		
		TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
		
		List<Trade> testTrades = new ArrayList<Trade>();
		testTrades.add(new Trade(stock, 95.33, 120, Trade.TradeType.BUY, strategy.getId()));
		testTrades.add(new Trade(stock, 12.33, 120, Trade.TradeType.SELL, strategy.getId()));
		
		when(mockTradeDao.findAllByState(stateToFind)).thenReturn(testTrades);
		List<Trade> tradeToFind = tradeService.findAllByState(stateToFind);
		
		verify(mockTradeDao).findAllByState(stateToFind);
		assertEquals(testTrades, tradeToFind);
	}
	
	@Test
	public void test_findAllRuns() {
		Stock stock = new Stock(-1, "GOOGL");
		TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
		
		List<Trade> testTrades = new ArrayList<Trade>();
		testTrades.add(new Trade(stock, 95.33, 120, Trade.TradeType.BUY, strategy.getId()));
		testTrades.add(new Trade(stock, 12.33, 120, Trade.TradeType.SELL, strategy.getId()));
		
		when(mockTradeDao.findAll()).thenReturn(testTrades);
		List<Trade> tradesToFind = tradeService.findAll();
		
		verify(mockTradeDao).findAll();
		assertEquals(testTrades, tradesToFind);
	}
}
