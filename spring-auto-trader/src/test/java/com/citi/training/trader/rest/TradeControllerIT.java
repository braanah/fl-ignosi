package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.strategy.TwoMovingAverages;

/**
 * Integration test for Stock REST Interface.
 *
 * Makes HTTP requests to {@link com.citi.training.TradeControllerTest.rest.TradeController}.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)

@AutoConfigureTestDatabase
@Transactional
public class TradeControllerIT {

    private static final Logger logger =
                        LoggerFactory.getLogger(TradeControllerIT.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${com.citi.training.trader.rest.trade-base-path:/trade}")
    private String tradeBasePath;
    
    @Value("${com.citi.training.trader.rest.two-moving-averages-base-path:/twoMovingAverages}")
    private String stratBasePath;
    
    @Test
    public void findAll_returnsList() {
        Stock stock = new Stock(-1, "AAPL");
		TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
		
		restTemplate.postForEntity(stratBasePath,
                strategy, TwoMovingAverages.class);
		
		Trade testTrade = new Trade(stock, 95.33, 120, Trade.TradeType.BUY, strategy.getId());
        
		restTemplate.postForEntity(tradeBasePath,
                                   testTrade, Trade.class);

        ResponseEntity<List<Trade>> findAllResponse = restTemplate.exchange(
                                tradeBasePath,
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<List<Trade>>(){});

        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
    }
//
//    @Test
//    public void findById_returnsCorrectId() {
//    	
//        Stock stock = new Stock(-1, "GOOGL");
//		TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
//    			50, 0, 0, 0, new Date());
//		
//		ResponseEntity<TwoMovingAverages> createdResponse1 = restTemplate.postForEntity(stratBasePath,
//                strategy, TwoMovingAverages.class);
//		
//		assertEquals(createdResponse1.getStatusCode(), HttpStatus.CREATED);
//		
//		Trade testTrade = new Trade(stock, 95.33, 120, Trade.TradeType.BUY, strategy.getId());
//		
//        ResponseEntity<Trade> createdResponse =
//                restTemplate.postForEntity(tradeBasePath,
//                                           testTrade, Trade.class);
//
//        assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);
//
//        Trade foundTrade = restTemplate.getForObject(
//                                tradeBasePath + "/" + createdResponse.getBody().getId(),
//                                Trade.class);
//
//        assertEquals(foundTrade.getId(), createdResponse.getBody().getId());
//        assert(foundTrade.getPrice() == testTrade.getPrice());
//    }
//
//    @Test
//    public void deleteById_deletes() {
//    	Stock stock = new Stock(-1, "GOOGL");
//		TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
//    			50, 0, 0, 0, new Date());
//		
//		restTemplate.postForEntity(stratBasePath,
//                strategy, TwoMovingAverages.class);
//
//		Trade testTrade = new Trade(stock, 95.33, 120, Trade.TradeType.BUY, strategy.getId());
//		
//        ResponseEntity<Trade> createdResponse =
//                restTemplate.postForEntity(tradeBasePath,
//                                           testTrade, Trade.class);
//
//        assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);
//
//        Trade foundTrade = restTemplate.getForObject(
//                                tradeBasePath + "/" + createdResponse.getBody().getId(),
//                                Trade.class);
//
//        logger.debug("Before delete, findById gives: " + foundTrade);
//        assertNotNull(foundTrade);
//
//        restTemplate.delete(tradeBasePath + "/" + createdResponse.getBody().getId());
//
//        ResponseEntity<Trade> response = restTemplate.exchange(
//                                tradeBasePath + "/" + createdResponse.getBody().getId(),
//                                HttpMethod.GET,
//                                null,
//                                Trade.class);
//
//        logger.debug("After delete, findById response code is: " +
//                     response.getStatusCode());
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//    }
}
