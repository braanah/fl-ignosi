package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class StockTests {

	@Test
	public void test_Constructor() {
		Stock stock = new Stock(1, "GOOGL");
    	
    	assertEquals(stock.getTicker(), "GOOGL");
    	assertEquals(stock.getId(), 1);
	}
}