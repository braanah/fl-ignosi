package com.citi.training.trader.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


import com.citi.training.trader.model.strategy.TwoMovingAverages;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.service.TwoMovingAveragesService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@WebMvcTest(TwoMovingAveragesController.class)
public class TwoMovingAveragesControllerTest {

    private static final Logger logger =
                LoggerFactory.getLogger(TwoMovingAveragesControllerTest.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TwoMovingAveragesService mockTwoMovingAveragesService;
    
    
    @Test
    public void findAllTwoMovingAveragess_returnsList() throws Exception {
        when(mockTwoMovingAveragesService.findAll())
            .thenReturn(new ArrayList<TwoMovingAverages>());

        MvcResult result = this.mockMvc
                .perform(get("/twoMovingAverages/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber())
                .andReturn();

        logger.info("Result from TwoMovingAveragesService.findAll: "
                    + result.getResponse().getContentAsString());
    }  
    
    @Test
    public void createTwoMovingAverages_returnsCreated() throws Exception {
        TwoMovingAverages testTwoMovingAverages = 
        		new TwoMovingAverages(1,new Stock(1, "FAT BOY"),1,1,1000,100,20,0, 0, 0, null);

        when(mockTwoMovingAveragesService
                .create(any(TwoMovingAverages.class)))
            .thenReturn(testTwoMovingAverages.getId());

        this.mockMvc.perform(
                post("/twoMovingAverages" )
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testTwoMovingAverages)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id")
                    .value(testTwoMovingAverages.getId()))
                .andReturn();
        logger.info("Result from Create TwoMovingAverages");
    }
    
    @Test
    public void deleteTwoMovingAveragesreturnsNoContent() throws Exception {
        MvcResult result = this.mockMvc
                .perform(delete("/twoMovingAverages" + "/1"))
                .andExpect(status().isNoContent())
                .andReturn();

        logger.info("Result from TwoMovingAveragesService.delete: "
                    + result.getResponse().getContentAsString());
    }
    
    @Test
    public void findTwoMovingAverages_returnsOk() throws Exception {
        MvcResult result = this.mockMvc
                .perform(get("/twoMovingAverages" + "/1"))
                .andExpect(status().isOk())
                .andReturn();

        logger.info("Result from TwoMovingAveragesService.findById: "
                    + result.getResponse().getContentAsString());
    }
    
}
