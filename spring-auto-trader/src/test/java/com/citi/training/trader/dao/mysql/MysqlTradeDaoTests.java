package com.citi.training.trader.dao.mysql;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.dao.TwoMovingAveragesDao;
import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.strategy.TwoMovingAverages;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class MysqlTradeDaoTests {

	Logger logger = LoggerFactory.getLogger(MysqlTradeDaoTests.class);
	
	@SpyBean
    JdbcTemplate tpl;

    @Autowired
    TradeDao tradeDao;
    
    @Autowired
	private StockDao stockDao;
    
    @Autowired
    private TwoMovingAveragesDao strategyDao;
    
    @Test
    @Transactional
    public void test_saveAndFindAll_works() {
    	Stock stock = new Stock(-1, "GOOGL");
    	stockDao.create(stock);
    	
    	TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
    	strategyDao.create(strategy);
    	
    	List<Trade> trades = new ArrayList<Trade>();
    	Trade trade1 = new Trade(stock, 95.33, 120, Trade.TradeType.BUY, strategy.getId());
    	Trade trade2 = new Trade(stock, 96.33, 120, Trade.TradeType.SELL, strategy.getId());
    	trades.add(trade1);
    	trades.add(trade2);
    	
    	tradeDao.save(trade1);
    	tradeDao.save(trade2);
    	
        assertEquals(tradeDao.findAll().get(0).getId(), trade1.getId());
        assertEquals(tradeDao.findAll().get(1).getId(), trade2.getId());
    }
    
    @Test
    @Transactional
    public void test_saveAndFindById_works() {
    	Stock stock = new Stock(-1, "GOOGL");
    	stockDao.create(stock);
    	
    	TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
    	strategyDao.create(strategy);
    	
    	List<Trade> trades = new ArrayList<Trade>();
    	Trade trade1 = new Trade(stock, 95.33, 120, Trade.TradeType.BUY, strategy.getId());
    	Trade trade2 = new Trade(stock, 96.33, 120, Trade.TradeType.SELL, strategy.getId());
    	trades.add(trade1);
    	trades.add(trade2);
    	
    	int firstId = tradeDao.save(trade1);
    	int secondId = tradeDao.save(trade2);
    	
        assertEquals(tradeDao.findById(firstId).getId(), trade1.getId());
        assertEquals(tradeDao.findById(secondId).getId(), trade2.getId());
    }

    @Test
    @Transactional
    public void test_saveAndFindByState_works() {
    	Stock stock = new Stock(-1, "GOOGL");
    	stockDao.create(stock);
    	
    	TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
    	strategyDao.create(strategy);
    	
    	List<Trade> trades = new ArrayList<Trade>();
    	Trade trade1 = new Trade(stock, 95.33, 120, Trade.TradeType.BUY, strategy.getId());
    	Trade trade2 = new Trade(stock, 96.33, 120, Trade.TradeType.SELL, strategy.getId());
    	trades.add(trade1);
    	trades.add(trade2);
    	
    	tradeDao.save(trade1);
    	tradeDao.save(trade2);
    	
        assertEquals(tradeDao.findAllByState(TradeState.INIT).get(0).getId(), trade1.getId());
        assertEquals(tradeDao.findAllByState(TradeState.INIT).get(1).getId(), trade2.getId());
    }
    
    @Test(expected=TradeNotFoundException.class)
    @Transactional
    public void test_saveAndDeleteById_throwsNotFound() {
    	Stock stock = new Stock(-1, "GOOGL");
    	stockDao.create(stock);
    	
    	TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
    	strategyDao.create(strategy);
    	
    	Trade trade1 = new Trade(stock, 96.33, 120, Trade.TradeType.SELL, strategy.getId());
    	
    	int newId = tradeDao.save(trade1);
    	
    	tradeDao.deleteById(newId);
    	tradeDao.findById(newId);
    }
}
