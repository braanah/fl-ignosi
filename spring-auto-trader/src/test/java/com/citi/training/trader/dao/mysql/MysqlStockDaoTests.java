package com.citi.training.trader.dao.mysql;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.exceptions.StockNotFoundException;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class MysqlStockDaoTests {
	Logger logger = LoggerFactory.getLogger(MysqlPriceDaoTests.class);
	
	@SpyBean
    JdbcTemplate tpl;

    @Autowired
	private StockDao stockDao;

    @Test
    @Transactional
    public void test_createAndFindAll_works() {
    	List<Stock> stocks = new ArrayList<Stock>();
    	stocks.add(new Stock(-1, "GOOGL"));
    	stocks.add(new Stock(-2, "BOGGL"));
    	
    	stockDao.create(stocks.get(0));
    	stockDao.create(stocks.get(1));
    	
        assertEquals(stockDao.findAll().get(0).getId(), stocks.get(0).getId());
        assertEquals(stockDao.findAll().get(1).getId(), stocks.get(1).getId());
    }
    
    @Test
    @Transactional
    public void test_createAndFindById_works() {
    	List<Stock> stocks = new ArrayList<Stock>();
    	stocks.add(new Stock(-1, "GOOGL"));
    	stocks.add(new Stock(-2, "BOGGL"));
    	
    	int firstId = stockDao.create(stocks.get(0));
    	int secondId = stockDao.create(stocks.get(1));
    	
        assertEquals(stockDao.findById(firstId).getId(), stocks.get(0).getId());
        assertEquals(stockDao.findById(secondId).getId(), stocks.get(1).getId());
    }
    
    @Test
    @Transactional
    public void test_createAndFindByTicker_works() {
    	List<Stock> stocks = new ArrayList<Stock>();
    	stocks.add(new Stock(-1, "GOOGL"));
    	stocks.add(new Stock(-2, "BOGGL"));
    	
    	stockDao.create(stocks.get(0));
    	stockDao.create(stocks.get(1));
    	
        assertEquals(stockDao.findByTicker("GOOGL").getId(), stocks.get(0).getId());
    }
    
    @Test(expected=StockNotFoundException.class)
    @Transactional
    public void test_createAndDeleteById_throwsNotFound() {
    	List<Stock> stocks = new ArrayList<Stock>();
    	stocks.add(new Stock(-1, "GOOGL"));
    	
    	int newId = stockDao.create(stocks.get(0));
    	stockDao.deleteById(newId);
    	
    	stockDao.findById(newId);
    }
}
