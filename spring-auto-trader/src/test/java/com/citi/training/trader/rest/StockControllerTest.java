package com.citi.training.trader.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


import com.citi.training.trader.model.Stock;
import com.citi.training.trader.service.StockService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@WebMvcTest(StockController.class)
public class StockControllerTest {

    private static final Logger logger =
                LoggerFactory.getLogger(StockControllerTest.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StockService mockStockService;
    
    
    @Test
    public void findAllStocks_returnsList() throws Exception {
        when(mockStockService.findAll())
            .thenReturn(new ArrayList<Stock>());

        MvcResult result = this.mockMvc
                .perform(get("/stock/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber())
                .andReturn();

        logger.info("Result from stockService.findAll: "
                    + result.getResponse().getContentAsString());
    }  
    
    @Test
    public void createStock_returnsCreated() throws Exception {
        Stock testStock = new Stock(1, "JAMMIN");

        when(mockStockService
                .create(any(Stock.class)))
            .thenReturn(testStock.getId());

        this.mockMvc.perform(
                post("/stock" )
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testStock)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id")
                    .value(testStock.getId()))
                .andReturn();
        logger.info("Result from Create Stock");
    }
    
    @Test
    public void deleteTrade_returnsNoContent() throws Exception {
        MvcResult result = this.mockMvc
                .perform(delete("/stock" + "/1"))
                .andExpect(status().isNoContent())
                .andReturn();

        logger.info("Result from TradeService.delete: "
                    + result.getResponse().getContentAsString());
    }
    
    @Test
    public void findTrade_returnsOk() throws Exception {
        MvcResult result = this.mockMvc
                .perform(get("/stock" + "/1"))
                .andExpect(status().isOk())
                .andReturn();

        logger.info("Result from TradeService.findById: "
                    + result.getResponse().getContentAsString());
    }
    
}
