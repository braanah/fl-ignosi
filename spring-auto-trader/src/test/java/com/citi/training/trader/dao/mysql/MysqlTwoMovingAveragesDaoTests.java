package com.citi.training.trader.dao.mysql;

import static org.junit.Assert.assertEquals;

import java.util.Date;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.dao.TwoMovingAveragesDao;
import com.citi.training.trader.exceptions.TwoMovingAveragesNotFoundException;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.strategy.TwoMovingAverages;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class MysqlTwoMovingAveragesDaoTests {

Logger logger = LoggerFactory.getLogger(MysqlTradeDaoTests.class);
	
	@SpyBean
    JdbcTemplate tpl;

    @Autowired
	private StockDao stockDao;
    
    @Autowired
    private TwoMovingAveragesDao strategyDao;
    
    @Test
    @Transactional
    public void test_createAndFindAll_works() {
    	Stock stock = new Stock(-1, "GOOGL");
    	stockDao.create(stock);
    	
    	TwoMovingAverages strategy1 = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
    	TwoMovingAverages strategy2 = new TwoMovingAverages(-1, stock, 120, 3, 20, 150,
    			70, 0, 0, 0, new Date());
    	strategyDao.create(strategy1);
    	strategyDao.create(strategy2);
    	
        assertEquals(strategyDao.findAll().get(0).getId(), strategy1.getId());
        assertEquals(strategyDao.findAll().get(1).getId(), strategy2.getId());
    }
    
    @Test
    @Transactional
    public void test_createAndFindById_works() {
    	Stock stock = new Stock(-1, "GOOGL");
    	stockDao.create(stock);
    	
    	TwoMovingAverages strategy1 = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
    	TwoMovingAverages strategy2 = new TwoMovingAverages(-1, stock, 120, 3, 20, 150,
    			70, 0, 0, 0, new Date());
    	int id1 = strategyDao.create(strategy1);
    	int id2 = strategyDao.create(strategy2);
    	
        assertEquals(strategyDao.findById(id1).getId(), strategy1.getId());
        assertEquals(strategyDao.findById(id2).getId(), strategy2.getId());
    }
    
    @Test(expected=TwoMovingAveragesNotFoundException.class)
    @Transactional
    public void test_createAndDeleteById_throwsNotFound() {
    	Stock stock = new Stock(-1, "GOOGL");
    	stockDao.create(stock);
    	
    	TwoMovingAverages strategy1 = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
    	int id1 = strategyDao.create(strategy1);
    	
    	strategyDao.deleteById(id1);
    	strategyDao.findById(id1);
    }
    
    @Test
    @Transactional
    public void test_createAndCloseStrategy_works() {
    	Stock stock = new Stock(-1, "GOOGL");
    	stockDao.create(stock);
    	
    	TwoMovingAverages strategy1 = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
    	int id1 = strategyDao.create(strategy1);
    	
    	strategyDao.closeStrategy(id1);
    	
    	assertEquals(strategyDao.findById(id1).getCurrentPosition(), 0);
    }
}
