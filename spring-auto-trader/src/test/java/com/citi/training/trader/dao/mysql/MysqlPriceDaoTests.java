package com.citi.training.trader.dao.mysql;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class MysqlPriceDaoTests {
	Logger logger = LoggerFactory.getLogger(MysqlPriceDaoTests.class);
	
	@SpyBean
    JdbcTemplate tpl;

    @Autowired
    PriceDao mysqlPriceDao;
    
    @Autowired
	private StockDao stockDao;

    @Test
    @Transactional
    public void test_createAndFindAll_works() {
    	Stock stock = new Stock(-1, "GOOGL");
    	Price price = new Price(-1, stock, 99.99, new Date());
    	List<Price> prices = new ArrayList<Price>();
    	prices.add(price);
    	
    	stock.setId(stockDao.create(stock));
        mysqlPriceDao.create(price);
        assertEquals(mysqlPriceDao.findAll(stock).get(0).getId(), prices.get(0).getId());
    }

    @Test
    @Transactional
    public void test_createAndFindLatest_works() {
    	
    	Stock google = new Stock(-1, "GOOGL");
    	google.setId(stockDao.create(google));
    	
    	Price priceForNow = new Price(-1, google, 99.99, new Date());
    	Price priceEarlier = new Price(-2, google, 12.99, new Date(124555));
    	
        mysqlPriceDao.create(priceForNow);
        mysqlPriceDao.create(priceEarlier);
        
        assertEquals(mysqlPriceDao.findLatest(google, 1).get(0).getId(), priceForNow.getId());
    }

    @Test
    @Transactional
    public void test_createAndFindAvgLatest_works() {
    	
    	Stock google = new Stock(-1, "GOOGL");
    	google.setId(stockDao.create(google));
    	
    	Price priceForNow = new Price(-1, google, 90, new Date());
    	Price priceEarlier = new Price(-2, google, 85, new Date(124555));
    	Price priceForNow2 = new Price(-3, google, 80, new Date());
    	Price priceEarlier2 = new Price(-4, google, 75, new Date(124555));
    	
        mysqlPriceDao.create(priceForNow);
        mysqlPriceDao.create(priceEarlier);
        mysqlPriceDao.create(priceForNow2);
        mysqlPriceDao.create(priceEarlier2);
        
        assert(mysqlPriceDao.findLatestAvg(google, 2) == 85);
    }

}
