package com.citi.training.trader.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2test")
@Transactional
public class StockServiceTests {
	
	@Autowired
	private StockService stockService;
	
	@MockBean
	private StockDao mockStockDao;
	
	@Test
	public void test_createRuns() {
		int newId = 1;
		
		Stock stock = new Stock(-1, "GOOGL");
		
		when(mockStockDao.create(stock)).thenReturn(newId);
		int createdId = stockService.create(stock);
		
		verify(mockStockDao).create(stock);
		assertEquals(newId, createdId);
	}

	@Test
	public void test_deleteRuns() {
		stockService.deleteById(4);
		
		verify(mockStockDao).deleteById(4);
	}
	
	@Test
	public void test_findByIdRuns() {
		int idToFind = 1;
		Stock testStock = new Stock(1, "HAM");
		
		when(mockStockDao.findById(idToFind)).thenReturn(testStock);
		Stock stockToFind = stockService.findById(idToFind);
		
		verify(mockStockDao).findById(idToFind);
		assertEquals(testStock, stockToFind);
	}
	
	@Test
	public void test_findByTickerRuns() {
		String tickerToFind = "HAM";
		Stock testStock = new Stock(1, "HAM");
		
		when(mockStockDao.findByTicker(tickerToFind)).thenReturn(testStock);
		Stock stockToFind = stockService.findByTicker(tickerToFind);
		
		verify(mockStockDao).findByTicker(tickerToFind);
		assertEquals(testStock, stockToFind);
	}
	
	@Test
	public void test_findAllRuns() {
		List<Stock> testStocks = new ArrayList<Stock>();
		testStocks.add(new Stock(1, "HAM"));
		testStocks.add(new Stock(2, "TOFU"));
		
		when(mockStockDao.findAll()).thenReturn(testStocks);
		List<Stock> stocksToFind = stockService.findAll();
		
		verify(mockStockDao).findAll();
		assertEquals(testStocks, stocksToFind);
	}


}
