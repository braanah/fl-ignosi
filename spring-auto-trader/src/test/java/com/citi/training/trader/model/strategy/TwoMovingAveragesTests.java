package com.citi.training.trader.model.strategy;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class TwoMovingAveragesTests {

	@Test
	public void test_Constructor() {
		Stock stock = new Stock(1, "GOOGL");
    	
    	TwoMovingAverages strategy = new TwoMovingAverages(1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
    	
    	assertEquals(strategy.getStock(), stock);
    	assertEquals(strategy.getStopped(), new Date());
    	assertEquals(strategy.getSize(), 120);
	}
}
