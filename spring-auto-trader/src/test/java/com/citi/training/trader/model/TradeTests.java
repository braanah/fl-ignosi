package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.model.strategy.TwoMovingAverages;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class TradeTests {

	@Test
	public void test_Constructor() {
		Stock stock = new Stock(-1, "GOOGL");
    	
    	TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
    	
    	Trade trade = new Trade(stock, 95.33, 120, Trade.TradeType.BUY, strategy.getId());
    	
    	assertEquals(trade.getStock(), stock);
    	assert(trade.getPrice() == 95.33);
    	assertEquals(trade.getStrategy(), strategy.getId());
    	assertEquals(trade.getSize(), 120);
	}
}
