package com.citi.training.trader.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.TwoMovingAveragesDao;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.strategy.TwoMovingAverages;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class TwoMovingAveragesServiceTests {
	@Autowired
	private StockService stockService;
	
	@Autowired
	private TwoMovingAveragesService strategyService;
	
	@MockBean
	private TwoMovingAveragesDao mockStrategyDao;
	
	@Test
	public void test_createRuns() {
		int newId = 1;
		
		Stock stock = new Stock(-1, "GOOGL");
		stockService.create(stock);
		
		TwoMovingAverages strategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
		
		when(mockStrategyDao.create(strategy)).thenReturn(newId);
		int createdId = strategyService.create(strategy);
		
		verify(mockStrategyDao).create(strategy);
		assertEquals(newId, createdId);
	}

	@Test
	public void test_deleteRuns() {
		strategyService.deleteById(4);
		
		verify(mockStrategyDao).deleteById(4);
	}
	
	@Test
	public void test_findByIdRuns() {
		int idToFind = 1;
		Stock stock = new Stock(-1, "AAPL");
		TwoMovingAverages testStrategy = new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date());
		
		when(mockStrategyDao.findById(idToFind)).thenReturn(testStrategy);
		TwoMovingAverages strategyToFind = strategyService.findById(idToFind);
		
		verify(mockStrategyDao).findById(idToFind);
		assertEquals(testStrategy, strategyToFind);
	}
	
	@Test
	public void test_findAllRuns() {

		Stock stock = new Stock(-1, "NFLX");

		
		List<TwoMovingAverages> testStrategies = new ArrayList<TwoMovingAverages>();
		
		testStrategies.add(new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date()));
		testStrategies.add(new TwoMovingAverages(-1, stock, 120, 3, 20, 100,
    			50, 0, 0, 0, new Date()));
		
		when(mockStrategyDao.findAll()).thenReturn(testStrategies);
		List<TwoMovingAverages> strategiesToFind = strategyService.findAll();
		
		
		assertEquals(testStrategies, strategiesToFind);
	}
	
}
