package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class PriceTests {

	@Test
	public void test_Constructor() {
		Stock stock = new Stock(1, "GOOGL");
    	Price price = new Price(1, stock, 99.99, new Date());
    	
    	assertEquals(price.getStock(), stock);
    	assertEquals(price.getId(), 1);
    	assert(price.getPrice()== 99.99);
    	assertEquals(price.getRecordedAt(), new Date());
	}
}
