package com.citi.training.trader.exceptions;

/**
 * An exception to be thrown by {@link com.citi.trading.dao.TwoMovingAveragesDao} implementations
 * when a requested strategy of that type is not found.
 *
 */
@SuppressWarnings("serial")
public class TwoMovingAveragesNotFoundException extends RuntimeException {
    public TwoMovingAveragesNotFoundException(String msg) {
        super(msg);
    }
}
