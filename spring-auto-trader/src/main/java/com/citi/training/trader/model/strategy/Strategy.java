package com.citi.training.trader.model.strategy;

import java.util.Date;

import com.citi.training.trader.model.Stock;

public interface Strategy {

	int getId();
	void setId(int id);
	
	Stock getStock();
	void setStock(Stock stock);
	
	int getSize();
	void setSize(int size);
	
	double getExitProfitLoss();
	void setExitProfitLoss(double exitProfitLoss);
	
	int getCurrentPosition();
	void setCurrentPosition(int currentPosition);
	
	double getProfit();
	void setProfit(double profit);
	
	Date getStopped();
	void setStopped(Date stopped);
	
	void addProfitLoss(double profitLoss);
	void stop();
	
	boolean hasPosition();
	boolean hasShortPosition();
	boolean hasLongPosition();
	void takeShortPosition();
	void takeLongPosition();
	void closePosition();
}
