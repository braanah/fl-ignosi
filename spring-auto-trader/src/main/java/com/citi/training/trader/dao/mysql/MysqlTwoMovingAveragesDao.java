package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.dao.TwoMovingAveragesDao;
import com.citi.training.trader.exceptions.TwoMovingAveragesNotFoundException;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.strategy.TwoMovingAverages;

/**
 * JDBC MySQL DAO implementation for two_moving_averages table.
 *
 */
@Component
public class MysqlTwoMovingAveragesDao implements TwoMovingAveragesDao {

    private static final Logger logger =
                            LoggerFactory.getLogger(MysqlTwoMovingAveragesDao.class);

    private static String FIND_ALL_SQL = "select two_moving_averages.id as strategy_id, stock.id as stock_id, stock.ticker, " +
                                         "size, exit_profit_loss, max_trades, long_avg, short_avg, price_when_position_opened, current_position, profit, stopped " +
                                         "from two_moving_averages left join stock on stock.id = two_moving_averages.stock_id";

    private static String INSERT_SQL = "INSERT INTO two_moving_averages (stock_id, size, exit_profit_loss, max_trades, long_avg, " + 
    									"short_avg, price_when_position_opened, current_position, profit, stopped) " +
                                       "values (:stock_id, :size, :exit_profit_loss, :max_trades, :long_avg, :short_avg, :price_when_position_opened, :current_position, " + 
                                       ":profit, :stopped)";

    private static String UPDATE_SQL = "UPDATE two_moving_averages set stock_id=:stock_id, size=:size, " +
                                       "exit_profit_loss=:exit_profit_loss, max_trades=:max_trades, long_avg=:long_avg, " + 
                                       "short_avg=:short_avg, price_when_position_opened=:price_when_position_opened, " +
                                       "current_position=:current_position, profit=:profit, stopped=:stopped where id=:id";

    private static String FIND_BY_ID_SQL = FIND_ALL_SQL + " where two_moving_averages.id = ?";
    
    private static String DELETE_BY_ID_SQL = "DELETE FROM two_moving_averages where id = ?";
    
    private static String CLOSE_STRATEGY_SQL = "UPDATE two_moving_averages set current_position=0, stopped=:stopped where id=:id";

    @Autowired
    private JdbcTemplate tpl;
    
    @Autowired
    private StockDao stockDao;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<TwoMovingAverages> findAll(){
        logger.debug("findAll SQL: [" + FIND_ALL_SQL + "]");
        return tpl.query(FIND_ALL_SQL,
                         new TwoMovingAveragesMapper());
    }

    public TwoMovingAverages findById(int id) {
        logger.debug("findById SQL: [" + FIND_BY_ID_SQL + "]");
        List<TwoMovingAverages> strategyList = 
        		tpl.query(FIND_BY_ID_SQL,
        				  new Object[]{id},
        				  new TwoMovingAveragesMapper());
        
        if( strategyList.size() <= 0 ) {
        	String excep = "Two Moving Averages strategy not found";
        	throw new TwoMovingAveragesNotFoundException(excep);
        }
        else {
        	return strategyList.get(0);
        }
        
    }
    
    public void deleteById(int id) {
        logger.debug("deleteById SQL: [" + DELETE_BY_ID_SQL + "]");
        if(tpl.update("delete from two_moving_averages where id = ?", id ) == 0) {
        	String excep = "Two Moving Averages strategy not found";
        	throw new TwoMovingAveragesNotFoundException(excep);
        }
    }
    
   
    
    public int create(TwoMovingAverages strategy) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        
        int stockId = strategy.getStock().getId();
        
        try{
        	stockDao.findById(stockId);
        		
        }catch(Exception e) {
        	logger.debug(e.toString());
        	stockId = stockDao.create(new Stock(stockId, strategy.getStock().getTicker()));
        }
           
        namedParameters.addValue("stock_id", stockId);
        namedParameters.addValue("size", strategy.getSize());
        namedParameters.addValue("exit_profit_loss", strategy.getExitProfitLoss());
        namedParameters.addValue("max_trades", strategy.getMaxTrades());
        namedParameters.addValue("long_avg", strategy.getLongAvgSeconds());
        namedParameters.addValue("short_avg", strategy.getShortAvgSeconds());
        namedParameters.addValue("price_when_position_opened", strategy.getPriceWhenPositionOpened());
        namedParameters.addValue("current_position", strategy.getCurrentPosition());
        namedParameters.addValue("profit", strategy.getProfit());
        namedParameters.addValue("stopped", strategy.getStopped());

       
        if(strategy.getId() <= 0) {
            logger.debug("Inserting TwoMovingAverages strategy: " + strategy);

            KeyHolder keyHolder = new GeneratedKeyHolder();

            namedParameterJdbcTemplate.update(INSERT_SQL, namedParameters, keyHolder);
            strategy.setId(keyHolder.getKey().intValue());
        } else {
            logger.debug("Updating TwoMovingAverages strategy: " + strategy);
            namedParameters.addValue("id", strategy.getId());
            namedParameterJdbcTemplate.update(UPDATE_SQL, namedParameters);
        }

        logger.debug("Saved TwoMovingAverages strategy: " + strategy);
        return strategy.getId();
    }

    public void closeStrategy(int id) {
    	MapSqlParameterSource namedParameters = new MapSqlParameterSource();
    	
    	TwoMovingAverages strategy = findById(id);
    	
    	strategy.stop();
    	namedParameters.addValue("stopped", strategy.getStopped());
    	
    	if(strategy.getId() > 0) {
            logger.debug("Closing TwoMovingAverages strategy: " + strategy);
            namedParameters.addValue("id", strategy.getId());
            namedParameterJdbcTemplate.update(CLOSE_STRATEGY_SQL, namedParameters);
        }
    }
    
    /**
     * private internal class to map database rows to TwoMovingAverages objects.
     *
     */
    private static final class TwoMovingAveragesMapper implements RowMapper<TwoMovingAverages> {
        public TwoMovingAverages mapRow(ResultSet rs, int rowNum) throws SQLException {
            logger.debug("Mapping TwoMovingAverages_strategy result set row num [" + rowNum + "], id : [" +
                    rs.getInt("strategy_id") + "]");

		       return new TwoMovingAverages(rs.getInt("strategy_id"),
		                        new Stock(rs.getInt("stock_id"),
		                                  rs.getString("stock.ticker")),
		                        rs.getInt("size"),
		                        rs.getDouble("exit_profit_loss"),
		                        rs.getInt("max_trades"),
		                        rs.getDouble("long_avg"),
		                        rs.getDouble("short_avg"),
		                        rs.getDouble("price_when_position_opened"),
		                        rs.getInt("current_position"),
		                        rs.getDouble("profit"),
		                        rs.getDate("stopped"));

		   }
    }
}
