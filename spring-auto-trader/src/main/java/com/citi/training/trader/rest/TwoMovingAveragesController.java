package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.service.TwoMovingAveragesService;
import com.citi.training.trader.model.strategy.TwoMovingAverages;

/**
 * REST Controller for {@link com.citi.training.trader.model.strategy.TwoMovingAverages} resource.
 *
 */
@CrossOrigin
@RestController
@RequestMapping("${com.citi.training.trader.rest.two-moving-averages-base-path:/twoMovingAverages}")
public class TwoMovingAveragesController {

    private static final Logger logger =
                    LoggerFactory.getLogger(TwoMovingAveragesController.class);

    @Autowired
    private TwoMovingAveragesService TwoMovingAveragesService;

    @CrossOrigin
    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<TwoMovingAverages> findAll() {
        logger.debug("findAll()");
        return TwoMovingAveragesService.findAll();
    }

    @CrossOrigin
    @RequestMapping(value="/{id}", method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public TwoMovingAverages findById(@PathVariable int id) {
        logger.debug("findById(" + id + ")");
        return TwoMovingAveragesService.findById(id);
    }
    
    @CrossOrigin
    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE,
    				produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TwoMovingAverages> create(@RequestBody TwoMovingAverages twoMovingAverages) {
        logger.debug("create(" + twoMovingAverages + ")");
        
        twoMovingAverages.setId(TwoMovingAveragesService.create(twoMovingAverages));
        logger.debug("created twoMovingAverages strategy: " + twoMovingAverages);
        
        return new ResponseEntity<TwoMovingAverages>(twoMovingAverages, HttpStatus.CREATED);
    }
    
    @CrossOrigin
    @RequestMapping(value="/close/{strategyId}", method=RequestMethod.PUT,
            consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> closeStrategy(@PathVariable int strategyId) {
		logger.debug("close strategy with id " +  strategyId);
		
		TwoMovingAveragesService.closeStrategy(strategyId);
		logger.debug("closing twoMovingAverages strategy: " + strategyId);
		
		return new ResponseEntity<Integer>(strategyId, HttpStatus.OK);
	}


    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        logger.debug("deleteById(" + id + ")");
        TwoMovingAveragesService.deleteById(id);
    }
}
