package com.citi.training.trader.model.strategy;

import java.util.Date;

import com.citi.training.trader.model.Stock;


public class TwoMovingAverages implements Strategy {

    private int id;
    private Stock stock;
    private int size;
    private double exitProfitLoss;
    private int maxTrades;
    private double longAvgSeconds;
    private double shortAvgSeconds;
    private double priceWhenPositionOpened;
	private int currentPosition;
    private double profit;
    private Date stopped;


	public TwoMovingAverages(int id, Stock stock, int size, double exitProfitLoss, int maxTrades, double longAvgSeconds,
			double shortAvgSeconds, double priceWhenPositionOpened, int currentPosition, double profit, Date stopped) {
		super();
		this.id = id;
		this.stock = stock;
		this.size = size;
		this.exitProfitLoss = exitProfitLoss;
		this.maxTrades = maxTrades;
		this.longAvgSeconds = longAvgSeconds;
		this.shortAvgSeconds = shortAvgSeconds;
		this.priceWhenPositionOpened = priceWhenPositionOpened;
		this.currentPosition = currentPosition;
		this.profit = profit;
		this.stopped = stopped;
	}

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double getExitProfitLoss() {
        return exitProfitLoss;
    }

    public void setExitProfitLoss(double exitProfitLoss) {
        this.exitProfitLoss = exitProfitLoss;
    }
    
	public int getMaxTrades() {
		return maxTrades;
	}

	public void setMaxTrades(int maxTrades) {
		this.maxTrades = maxTrades;
	}
    
    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public Date getStopped() {
        return this.stopped;
    }

    public void setStopped(Date stopped) {
        this.stopped = stopped;
    }

    public void addProfitLoss(double profitLoss) {
    	double newProfit = this.profit;
        newProfit += (Math.round(profitLoss * 100.0) / 100.0);
        this.profit = Math.round(newProfit * 100.0) / 100.0;
    }

    public void stop() {
        this.stopped = new Date();
    }

    public int getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(int currentPosition) {
		this.currentPosition = currentPosition;
	}

	public boolean hasPosition() {
        return this.currentPosition != 0;
    }

    public boolean hasShortPosition() {
        return this.currentPosition < 0;
    }

    public boolean hasLongPosition() {
        return this.currentPosition > 0;
    }

    public void takeShortPosition() {
        this.currentPosition = -1;
    }

    public void takeLongPosition() {
        this.currentPosition = 1;
    }

    public void closePosition() {
        this.currentPosition = 0;
    }

	public double getLongAvgSeconds() {
		return longAvgSeconds;
	}

	public void setLongAvgSeconds(double longAvgSeconds) {
		this.longAvgSeconds = longAvgSeconds;
	}

	public double getShortAvgSeconds() {
		return shortAvgSeconds;
	}

	public void setShortAvgSeconds(double shortAvgSeconds) {
		this.shortAvgSeconds = shortAvgSeconds;
	}

    public double getPriceWhenPositionOpened() {
		return priceWhenPositionOpened;
	}

	public void setPriceWhenPositionOpened(double priceWhenPositionOpened) {
		this.priceWhenPositionOpened = priceWhenPositionOpened;
	}
	
	
	@Override
	public String toString() {
		return "TwoMovingAverages [id=" + id + ", stock=" + stock + ", size=" + size + ", exitProfitLoss="
				+ exitProfitLoss + ", maxTrades=" + maxTrades + ", longAvgSeconds=" + longAvgSeconds
				+ ", shortAvgSeconds=" + shortAvgSeconds + ", priceWhenPositionOpened=" + priceWhenPositionOpened
				+ ", currentPosition=" + currentPosition + ", profit=" + profit + ", stopped=" + stopped + "]";
	}


 

}
