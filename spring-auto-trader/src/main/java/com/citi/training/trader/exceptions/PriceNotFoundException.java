package com.citi.training.trader.exceptions;

/**
 * An exception to be thrown by {@link com.citi.trading.dao.PriceDao} implementations
 * when a requested price is not found. Used for testing purposes.
 *
 */
@SuppressWarnings("serial")
public class PriceNotFoundException extends RuntimeException {
    public PriceNotFoundException(String msg) {
        super(msg);
    }
}
