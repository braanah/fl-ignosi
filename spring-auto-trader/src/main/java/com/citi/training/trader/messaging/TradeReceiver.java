package com.citi.training.trader.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.strategy.TwoMovingAverages;
import com.citi.training.trader.service.StockService;
import com.citi.training.trader.service.TradeService;
import com.citi.training.trader.service.TwoMovingAveragesService;

/**
 * Service class that will listen on JMS queue for Trades and put
 * returned trades into the database.
 *
 */
@Component
public class TradeReceiver {

    private static final Logger logger =
                    LoggerFactory.getLogger(TradeReceiver.class);

    @Autowired
    private TradeService tradeService;

    @Autowired
    private StockService stockService;
    
    @Autowired
    private TwoMovingAveragesService strategyService;

    @JmsListener(destination = "OrderBroker_Reply")
    public void receiveTrade(String xmlReply) {

        logger.debug("Processing OrderBroker Replies");

        Trade tradeReply = Trade.fromXml(xmlReply);


        tradeReply.setStock(stockService.findByTicker(tradeReply.getTempStockTicker()));

        if (xmlReply.contains("<result>REJECTED</result>")) {
            tradeReply.stateChange(Trade.TradeState.REJECTED);
            Trade tradeReplyInDB = tradeService.findById(tradeService.save(tradeReply));

            //rejected trades should not be adding to the profit loss
            TwoMovingAverages currentStrategy = strategyService.findById(tradeReplyInDB.getStrategy());
            double profitLossToBeSubtracted = tradeReply.getPrice() - currentStrategy.getPriceWhenPositionOpened();
            currentStrategy.addProfitLoss(-1.0 * profitLossToBeSubtracted);
            
            logger.debug("Parsed returned trade: ");
            logger.debug(Trade.toXml(tradeReply));

        } else if (xmlReply.contains("<result>Partially_Filled</result>")) {
            tradeReply.stateChange(Trade.TradeState.INIT);
            tradeService.save(tradeReply);
            logger.debug("Parsed returned trade: ");
            logger.debug(Trade.toXml(tradeReply));
            
        } else {
            tradeReply.stateChange(Trade.TradeState.FILLED);
            tradeService.save(tradeReply);
            logger.debug("Parsed returned trade: ");
            logger.debug(Trade.toXml(tradeReply));
        }
    }
}
