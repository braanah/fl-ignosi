package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.strategy.TwoMovingAverages;

public interface TwoMovingAveragesDao {

    List<TwoMovingAverages> findAll();
    
    TwoMovingAverages findById(int id);
    
    void deleteById(int id);

    int create(TwoMovingAverages strategy);
    
    void closeStrategy(int id);
}
