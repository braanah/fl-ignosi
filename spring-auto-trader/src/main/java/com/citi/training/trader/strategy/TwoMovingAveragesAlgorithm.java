package com.citi.training.trader.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.dao.TwoMovingAveragesDao;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.strategy.TwoMovingAverages;

@Component
public class TwoMovingAveragesAlgorithm implements StrategyAlgorithm {

    private static final Logger logger =
                LoggerFactory.getLogger(TwoMovingAveragesAlgorithm.class);

    @Autowired
    private PriceDao priceDao;

    @Autowired
    private TradeSender tradeSender;

    
    @Autowired
    private TwoMovingAveragesDao strategyDao;

    @EventListener(ApplicationReadyEvent.class)
    public void closePositionsAfterStartup() {
        for(TwoMovingAverages strategy : strategyDao.findAll()) {
        	strategy.closePosition();
        	strategyDao.create(strategy);
        }
    }
    
    @Scheduled(fixedRateString = "${simple.strategy.refresh.rate_ms:5000}")
    public void run() {

        for(TwoMovingAverages strategy: strategyDao.findAll()) {
            if(strategy.getStopped() != null) {
                continue;
            }
            
            if(!strategy.hasPosition()) {
                // we have no open position

            	int numPriceEntriesForLongAvg = (int)strategy.getLongAvgSeconds() / 15;
            	int numPriceEntriesForShortAvg = (int)strategy.getShortAvgSeconds() / 15;
            	
            	double currentLongAverage = priceDao.findLatestAvg(strategy.getStock(), numPriceEntriesForLongAvg);
            	double currentShortAverage = priceDao.findLatestAvg(strategy.getStock(), numPriceEntriesForShortAvg);

            	double previousLongAverage = priceDao.findPreviousLatestAvg(strategy.getStock(), numPriceEntriesForLongAvg);
            	double previousShortAverage = priceDao.findPreviousLatestAvg(strategy.getStock(), numPriceEntriesForShortAvg);
            	
                if(previousLongAverage == -1.0) {
                    logger.warn("Unable to execute strategy, not enough price data: " +
                                strategy); 
                    continue;
                }

                logger.debug("Taking position based on prices:");

            	boolean noCrossBetweenLongShort = ((currentLongAverage > currentShortAverage) && (previousLongAverage > previousShortAverage)) ||
            			((currentLongAverage < currentShortAverage) && (previousLongAverage < previousShortAverage));
            	boolean shortAvgCrossesAbove = ((currentLongAverage < currentShortAverage) && (previousLongAverage > previousShortAverage));
            	boolean shortAvgCrossesBelow = ((currentLongAverage > currentShortAverage) && (previousLongAverage < previousShortAverage));
            	logger.debug("\nDidn't cross at all: " + noCrossBetweenLongShort + " " +
            				"\nShort crossed long going up: " + shortAvgCrossesAbove + " " +
            				"\nShort crossed long going down: " + shortAvgCrossesBelow);
            	if(noCrossBetweenLongShort == true) {
                    logger.debug("Insufficient price change, taking no action");
                    continue;
                }
                if(shortAvgCrossesBelow == true) {
                    logger.debug("Taking short position for strategy: " + strategy);
                    strategy.takeShortPosition();
                    strategy.setPriceWhenPositionOpened(makeTrade(strategy, Trade.TradeType.SELL));
                } else if(shortAvgCrossesAbove == true) {
                    // if short average crosses above long average => take long => buy
                    logger.debug("Taking long position for strategy: " + strategy);
                    strategy.takeLongPosition();
                    strategy.setPriceWhenPositionOpened(makeTrade(strategy, Trade.TradeType.BUY));
                }

            } else if(strategy.hasLongPosition()) {
                // we have a long position on this stock
                // close the position by selling if profit or loss is at the prescribed percentage

            	double thisTradePrice  = priceDao.findLatest(strategy.getStock(), 1).get(0).getPrice();
                double profitLossPercentage = ((thisTradePrice - strategy.getPriceWhenPositionOpened()) / strategy.getPriceWhenPositionOpened()) * 100;
                
            	if(Math.abs(profitLossPercentage) >= strategy.getExitProfitLoss()) {
            		logger.debug("Closing long position for strategy: " + strategy);
	                logger.debug("Bought at: " + strategy.getPriceWhenPositionOpened() + ", sold at: " +
	                             thisTradePrice);
	                thisTradePrice = makeTrade(strategy, Trade.TradeType.SELL);
	                double profitLoss = Math.round((thisTradePrice - strategy.getPriceWhenPositionOpened()) * 100.0) / 100.0;
	                closePosition(profitLoss, strategy);
            	}

            } else if(strategy.hasShortPosition()) {
                // we have a short position on this stock
                // close the position by buying if profit or loss is at the prescribed percentage
                
            	double thisTradePrice  = priceDao.findLatest(strategy.getStock(), 1).get(0).getPrice();
                double profitLossPercentage = ((thisTradePrice - strategy.getPriceWhenPositionOpened()) / strategy.getPriceWhenPositionOpened()) * 100;
                
            	if(Math.abs(profitLossPercentage) >= strategy.getExitProfitLoss()) {
	                logger.debug("Closing short position for strategy: " + strategy);
	                logger.debug("Sold at: " + strategy.getPriceWhenPositionOpened() + ", bought at: " +
	                             thisTradePrice);
	                thisTradePrice = makeTrade(strategy, Trade.TradeType.BUY);
	                double profitLoss = Math.round((thisTradePrice - strategy.getPriceWhenPositionOpened()) * 100.0) / 100.0;
	                closePosition(profitLoss, strategy);
            	}
            }
            strategyDao.create(strategy);
        }
    }

    private void closePosition(double profitLoss, TwoMovingAverages strategy) {
        logger.debug("Recording profit/loss of: " + profitLoss +
                     " for strategy: " + strategy);
        strategy.addProfitLoss(profitLoss);
        strategy.closePosition();
    }


    private double makeTrade(TwoMovingAverages strategy, Trade.TradeType tradeType) {
        Price currentPrice = priceDao.findLatest(strategy.getStock(), 1).get(0);
        
        Trade trade = new Trade(strategy.getStock(), currentPrice.getPrice(),
                strategy.getSize(), tradeType,
                strategy.getId());

        tradeSender.sendTrade(trade);

        return currentPrice.getPrice();
    }

}
