package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TwoMovingAveragesDao;
import com.citi.training.trader.model.strategy.TwoMovingAverages;

@Component
public class TwoMovingAveragesService {
	
	@Autowired
	private TwoMovingAveragesDao strategyDao;
	
	public int create(TwoMovingAverages strategy) {
		return strategyDao.create(strategy);
	}
	
	public List<TwoMovingAverages> findAll() {
		return strategyDao.findAll();
	}
	
	public TwoMovingAverages findById(int id) {
		return strategyDao.findById(id);
	}
	
	public void deleteById(int id) {
		strategyDao.deleteById(id);
	}
	
	public void closeStrategy(int strategyId) {
		strategyDao.closeStrategy(strategyId);
	}
}
