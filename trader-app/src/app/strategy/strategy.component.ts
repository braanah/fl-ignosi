import { Component, OnInit } from '@angular/core';
import { Strategy, StrategyService } from '../common/strategy-service/strategy.service';

@Component({
  selector: 'app-strategy',
  templateUrl: './strategy.component.html',
  styleUrls: ['./strategy.component.css']
})
export class StrategyComponent implements OnInit {

  strategies: Strategy[];
  isLoading: boolean = true;

  constructor(private strat: StrategyService) {
    this.strategies = [];

  }

  ngOnInit() {
    this.strat.getStrategies().subscribe(response => {

      this.strategies = response;
      this.isLoading = false;
    });

  }

  stopStrategy(id: number) {
    this.strat.stopStrategy(id).subscribe(response => {
      console.log("stop strategy:", response)
      location.reload();
    });
  }

}
