import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LoggerService } from '../logger.service';
import { environment } from '../../../environments/environment';

export class Trade {
  id: number;
  price: number;
  size: number;
  strategy: number;
  lastStateChange: Date;
  tradeType: string;
  state: string;
  stockTicker: string;
}

@Injectable({
  providedIn: 'root'
})
export class TradeService {

  // baseUrl: string = environment.apiUrl;

  constructor(private httpClient: HttpClient, private log: LoggerService) { }

  getTrades(): Observable<Trade[]> {

    this.log.log('TradeService', `returning a collection of trades`)
    return this.httpClient.get<Trade[]>('/trade')
  }
}
