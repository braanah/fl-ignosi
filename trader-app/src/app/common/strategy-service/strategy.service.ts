import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { LoggerService } from '../logger.service';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

export class Strategy {
  id: number;
  stock: Stock;
  size: number;
  exitProfitLoss: number;
  maxTrades: number;
  longAvgSeconds: number;
  shortAvgSeconds: number;
  priceWhenPositionOpened: number;
  currentPosition: number;
  profit: number;
  stopped: Date;
}

export class Stock {
  id: number;
  ticker: string;
}


@Injectable({
  providedIn: 'root'
})
export class StrategyService {

  // baseUrl: string = environment.apiUrl;

  constructor(private httpClient: HttpClient, private log: LoggerService) { }

  headers = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  getStrategies(): Observable<Strategy[]> {
    this.log.log('StrategyService', `returning a collection of strategies`)
    return this.httpClient.get<Strategy[]>('/twoMovingAverages');
  }

  setStrategy(strategy: Strategy): Observable<Strategy> {
    this.log.log('StrategyService', `generating a strategy`);
    return this.httpClient.post<Strategy>('/twoMovingAverages', JSON.stringify(strategy), this.headers);

  }

  stocks: Stock[] = [];

  getStocks(): Observable<Stock[]> {
    let id: number;
    this.log.log('StrategyService', `getting stocks`);
    return this.httpClient.get<Stock[]>('/stock');
  }

  stopStrategy(stratID: number) {
    let date = new Date();
    this.log.log('StrategyService', `stopping strategy ${stratID}`);
    return this.httpClient.put(`/twoMovingAverages/close/${stratID}`, {
      "stopped": new Date()
    }, this.headers);

  }
}
