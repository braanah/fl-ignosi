import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './form/form.component';
import { StrategyComponent } from './strategy/strategy.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent }, // default route
  { path: 'home', component: HomeComponent },
  { path: 'strategy', component: StrategyComponent },
  { path: 'home/strategy', component: StrategyComponent },
  { path: 'form', component: FormComponent },
  { path: 'home/form', component: FormComponent },
  { path: 'strategy/form', component: FormComponent },
  { path: '**', redirectTo: '/home' },
  { path: '*/error', redirectTo: '/strategy' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
