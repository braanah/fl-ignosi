import { Component, OnInit } from '@angular/core';
import { StrategyService, Strategy, Stock } from '../common/strategy-service/strategy.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  strategy: Strategy;
  stocks: Stock[] = [];

  constructor(
    private strat: StrategyService) {
    this.strat.getStocks().subscribe(response => {

      this.stocks = response;
      console.log(response);
    });
  }

  ngOnInit() {
    this.strategy = new Strategy;
    this.strategy.stock = new Stock;
  }

  createStrategy(strategy) {
    // this.getStockID(this.strategy.stock.ticker);
    this.strategy.id = -1;
    strategy = this.strategy;
    this.strat.setStrategy(strategy).subscribe(response => { console.log("Response from post", response); }

    );
    console.log(strategy);
  }

  // getStockID(stockName: string) {
  //   var exists = this.stocks.some(stock => stock.ticker === stockName);
  //   console.log(exists);

  //   if (exists) {
  //     var obj = this.stocks.find(function (obj) { return obj.ticker === stockName; });
  //     this.strategy.stock.id = obj.id;
  //   }
  //   console.log("getStockID returns: ", this.strategy.stock.id);
  // }

}
