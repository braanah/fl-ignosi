import { Component, OnInit, Input } from '@angular/core';
import { Trade, TradeService } from '../common/trade-service/trade.service'
import { Chart } from 'node_modules/chart.js'
import { Strategy, StrategyService } from '../common/strategy-service/strategy.service';

@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.css']
})
export class TradeComponent implements OnInit {

  @Input()
  strategy: Strategy;

  trades: Trade[];
  strategySpecificTrades: Trade[];
  strategySpecificProfits: number[];
  prices: number[];
  dates: string[];
  strategies: Strategy[];

  constructor(private ts: TradeService, private strat: StrategyService) { }

  setProfitData() {
    console.log("Strategy:", this.strategy.id);

    this.prices.push(this.strategy["profit"]);
    let n = new Date();
    let fin = n.toLocaleString();
    this.dates.push(fin);
    console.log("Profits:", this.prices);
  }

  setGraph() {
    var ctx = document.getElementById('myChart' + this.strategy.id);
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: this.dates,
        datasets: [{
          label: 'Profit',
          data: this.prices,
          backgroundColor: [
            'rgb(0, 255, 0, 0.2)'
          ],
          borderColor: [
            'rgb(0, 255, 0, 1)'
          ],
          borderWidth: 1,
          pointRadius: 0
        }]
      },
      options: {

        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: false
            }
          }],
          xAxes: [{
            ticks: {
              display: false //this will remove only the label
            }
          }]
        }
      }
    });
  }

  setStrategySpecificTrades(allTrades: Trade[]) {
    this.strategySpecificTrades = [];
    this.strategySpecificTrades = allTrades.filter(x => x.strategy == this.strategy.id);
  }

  getData() {
    this.ts.getTrades().subscribe(response => {

      this.trades = response;
      this.setStrategySpecificTrades(this.trades);
    });

    this.strat.getStrategies().subscribe(response => {

      this.strategies = response;
      for (let strat of this.strategies) {

        if (this.strategy.id == strat.id) {
          this.strategy.profit = strat.profit;
        }

      }
      this.setProfitData();
      this.setGraph();
    });
  }

  ngOnInit() {
    console.log("STRAT: ", this.strategy);
    this.prices = [];
    this.dates = [];

    this.getData();

    setInterval(() => {
      this.getData();
      this.setProfitData();
      this.setGraph();
    }, 15000);
  }
}
